require "sinatra"
require "slim"

set :public_folder, "assets"
set :views, "templates"
get("/styles.min.css"){ scss :styles }

get "/" do
  slim :home
end
